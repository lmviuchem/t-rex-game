import pygame,random


pygame.init()

pantalla = pygame.display.set_mode((1280, 720))                     #Tamaño De La Ventana

pygame.display.set_caption("Juego Prron")                           #Nombre Del Juego

logo = pygame.image.load("mario_assets\iconlogo.png")               #Icono Logo 
pygame.display.set_icon(logo)


fuente = pygame.font.Font("mario_assets\Lemon Tea.ttf", 35)         #Fuente del juego

#Creacion de Clases

class Nube(pygame.sprite.Sprite):                                   #Creacion Clase Nube
    def __init__(self, image, x_pos, y_pos):                        #Atributos,parametros ( imagen, posicion) FUNCION
        super().__init__()
        self.image = image
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))

    def update(self):                                               #Funcion actualizar
        self.rect.x -= 1


class Mario(pygame.sprite.Sprite):                                  #Creacion Clase Mario
    def __init__(self, x_pos, y_pos):                               #Atributos (posicion)
        super().__init__()
        self.lista_correr   =       []                              #lista de mario corriendo para la tranformacion de imagen
        self.lista_agachar  =       []                              #lista de mario agachado para la tranformacion de imagen

        self.lista_correr.append(
            pygame.transform.scale(pygame.image.load("mario_assets\Mario\mario1.png"),
                                   (80, 100)))
        self.lista_correr.append(
            pygame.transform.scale(pygame.image.load("mario_assets\Mario\mario2.png"),
                                   (80, 100)))

        self.lista_agachar.append(
            pygame.transform.scale(
                pygame.image.load(f"mario_assets\Mario\mario_agachado1.png"), (80, 60)))
        self.lista_agachar.append(
            pygame.transform.scale(
                pygame.image.load(f"mario_assets\Mario\mario_agachado2.png"), (80, 60)))

        self.x_pos = x_pos
        self.y_pos = y_pos
        self.current_image = 0
        self.image = self.lista_correr[self.current_image]
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))
        self.velocity = 50
        self.gravity = 4.5
        self.agacharing = False                                       #Modo agachado desactivado si no se acciona

    def salto(self):                                                  #Funcion de saltar
        salto_sonido.play()                                           #Sonido de saltar
        if self.rect.centery >= 360:
            while self.rect.centery - self.velocity > 40:
                self.rect.centery -= 1

    def agachar(self):                                                                  #Funcion de saltar
        self.agacharing = True
        self.rect.centery = 380

    def noagachar(self):                                                                #Funcion para predeterminar caminar
        self.agacharing = False
        self.rect.centery = 360

    def gravedad(self):                                                                 #Funcion para gravedad
        if self.rect.centery <= 360:
            self.rect.centery += self.gravity

    def update(self):                                                                   #Funcion para actualizar el personaje
        self.animate()
        self.gravedad()

    def animate(self):                                                                  #Funcion para animar el personaje
        self.current_image += 0.05
        if self.current_image >= 2:
            self.current_image = 0

        if self.agacharing:
            self.image = self.lista_agachar[int(self.current_image)]
        else:
            self.image = self.lista_correr[int(self.current_image)]


class Tuberias(pygame.sprite.Sprite):                                                   #Clase Tuberia (obstaculos)
    def __init__(self, x_pos, y_pos):                                                   #Funcion para definir posicion
        super().__init__()
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.obstaculos = []
        for i in range(1, 7):                                                           #ciclo for para pasa las imagenes de la tuberia
            current_sprite = pygame.transform.scale(
                pygame.image.load(f"mario_assets\Obstaculos\cactus_{i}.png"), (70, 90)) #Imagenes dentro de los assets de los obstaculos
            self.obstaculos.append(current_sprite)                                      
        self.image = random.choice(self.obstaculos)                                     #randomiza el obstaculo
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))                #posicion del obstaculo

    def update(self):                                                                   #refresca el obstaculo
        self.x_pos -= velocidad
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))


class Pajaro(pygame.sprite.Sprite):                                                     #Clase Pajaro (obstaculo aereo)
    def __init__(self):                                                                 #funcion para inicializar el pajaro
        super().__init__()
        self.x_pos = 1300                                                               #posicion en x del pajaro
        self.y_pos = random.choice([290, 350, 340])                                     #posicion en y del pajaro randomizada
        self.pajaros = []                                                               #lista para ingresar las imagenes de los pajaros
        self.pajaros.append(                                                                        #Agregar a la lista de imagenes
            pygame.transform.scale(pygame.image.load("mario_assets\Obstaculos\pajaro1.png"),
                                   (70, 72)))
        self.pajaros.append(
            pygame.transform.scale(pygame.image.load("mario_assets\Obstaculos\pajaro2.png"),
                                   (70, 72)))
        self.current_image = 0  
        self.image = self.pajaros[self.current_image]
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))

    def update(self):                                                                   #refrescar la imagen del pajaro
        self.animate()
        self.x_pos -= velocidad
        self.rect = self.image.get_rect(center=(self.x_pos, self.y_pos))

    def animate(self):                                                                  #animacion del pajaro cambiando de imagen
        self.current_image += 0.025
        if self.current_image >= 2:
            self.current_image = 0
        self.image = self.pajaros[int(self.current_image)]


#VARIABLES

velocidad = 5
altura_salto = 10
puntaje = 0
final = False
tiempo_obstaculo = 0
frecuencia_spawn = False
tiempo_entre = 1000

#FONDOS

piso = pygame.image.load("mario_assets\Fondo\piso.png")
piso = pygame.transform.scale(piso, (1280,60))
piso_x = 0
piso_rect = piso.get_rect(center=(640, 400))

nube = pygame.image.load("mario_assets\Fondo\mnube.png")
nube = pygame.transform.scale(nube, (200, 80))

#AGRUPACION DE TODO

nube_grupo = pygame.sprite.Group()
obstaculo_grupo = pygame.sprite.Group()
mario_grupo = pygame.sprite.GroupSingle()
pajaro_grupo = pygame.sprite.Group()

#OBJETOS
personaje = Mario(50, 360)
mario_grupo.add(personaje)

#SONIDOS
muerte_sonido = pygame.mixer.Sound("mario_assets\sonidos mario\Muerte.mp3")
puntos_sonido = pygame.mixer.Sound("mario_assets\sonidos mario\puntos.mp3")
salto_sonido = pygame.mixer.Sound("mario_assets\sonidos mario\salto.mp3")

#EVENTOS
nube_evento = pygame.USEREVENT
pygame.time.set_timer(nube_evento, 2500)

#FUNCIONES


def acabarjuego():
    global puntaje, velocidad
    texto_final = fuente.render("Juego Finalizado!", True, "black")
    final_rect = texto_final.get_rect(center=(640, 250))
    puntuacion_texto = fuente.render(f"Tu puntuacion es: {int(puntaje)}", True, "black")
    score_rect = puntuacion_texto.get_rect(center=(640, 340))
    pantalla.blit(texto_final, final_rect)
    pantalla.blit(puntuacion_texto, score_rect)
    velocidad = 5
    nube_grupo.empty()
    obstaculo_grupo.empty()


while True:
    keys = pygame.key.get_pressed()
    if keys[pygame.K_DOWN]:
        personaje.agachar()
    else:
        if personaje.agacharing:
            personaje.noagachar()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if event.type == nube_evento:
            current_nube_y = random.randint(50, 300)
            current_nube = Nube(nube, 1380, current_nube_y)
            nube_grupo.add(current_nube)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE or event.key == pygame.K_UP:
                personaje.salto()
                if final:
                    final = False
                    velocidad = 5
                    puntaje = 0

    pantalla.fill("#B3CAC9")

    #COLISIONES
    if pygame.sprite.spritecollide(mario_grupo.sprite, obstaculo_grupo, False):
        final = True
        muerte_sonido.play()
    if final:
        acabarjuego()

    if not final:
        velocidad += 0.0025
        if round(puntaje, 1) % 100 == 0 and int(puntaje) > 0:
            puntos_sonido.play()

        if pygame.time.get_ticks() - tiempo_obstaculo >= tiempo_entre:
            frecuencia_spawn = True

        if frecuencia_spawn:
            obstacle_random = random.randint(1, 50)
            if obstacle_random in range(1, 7):
                new_obstacle = Tuberias(1280, 360)
                obstaculo_grupo.add(new_obstacle)
                tiempo_obstaculo = pygame.time.get_ticks()
                frecuencia_spawn = False
            elif obstacle_random in range(7, 10):
                new_obstacle = Pajaro()
                obstaculo_grupo.add(new_obstacle)
                tiempo_obstaculo = pygame.time.get_ticks()
                frecuencia_spawn = False

        puntaje += 0.1
        puntaje_surface = fuente.render(str(int(puntaje)), True,
                                                ("black"))
        pantalla.blit(puntaje_surface, (1150, 20))

        nube_grupo.update()
        nube_grupo.draw(pantalla)

        pajaro_grupo.update()
        pajaro_grupo.draw(pantalla)

        mario_grupo.update()
        mario_grupo.draw(pantalla)

        obstaculo_grupo.update()
        obstaculo_grupo.draw(pantalla)

        piso_x -= velocidad

        pantalla.blit(piso, (piso_x, 400))
        pantalla.blit(piso, (piso_x + 1280, 400))

        if piso_x <= -1280:
            piso_x = 0

    clock = pygame.time.Clock()
    clock.tick(120)

    pygame.display.update()